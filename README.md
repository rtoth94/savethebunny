# SaveTheBunny

**Save the Bunny - Mechanics**

*  Rabbit Actions
* Turn into direction
* Die
* Jump through hole
* Wait - Idle

**Map objects**

* Carrot: Collectable
* Golden Carrot - Advantages
* Obstacle: Wall and Box
* Black hole: Instant death
* Bush: Stealth

**NPC**
* Eagle: turn based control into given direction


**Save the Bunny - Work**
* Create grid movement
* Create coding elements as UI element
* Create GUI for build the commands
* Create interpreter of coding elements
* Create the mechanics
* Final animation


# Progress
---

* https://www.youtube.com/watch?v=AGbmpXct0mk&feature=youtu.be&fbclid=IwAR1rHPM2NN4RAKX4KQ0TPQP71EInA0vybT1XW_QCF22dH7pAfujbsQGAn6A


