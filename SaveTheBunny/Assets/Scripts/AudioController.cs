﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioClip move;
    public AudioClip fail;
    public AudioClip fall_down;
    public AudioClip eat;

    public AudioSource main_;
    public AudioSource bunny_source_;
    private AudioSource music_source_;

    public AudioClip[] musics_;

    private void Awake()
    {
        music_source_ = this.GetComponent<AudioSource>();
    }

    void Start()
    {
    }

    void Update()
    {
        
    }

    public void PlayBackgroundMusic(int index) 
    {
        music_source_.clip = musics_[index];
        music_source_.Play();
    }

    public void PlayMoveSound() 
    {
        main_.clip = move;
        main_.Play();
    }

    public void PlayFailedSound()
    {
        main_.clip = fail;
        main_.Play();
    }

    public void PlayFallSound()
    {
        main_.clip = fall_down;
        main_.Play();
    }

    public void PlayEatSound()
    {
        bunny_source_.clip = eat;
        bunny_source_.Play();
    }
}
