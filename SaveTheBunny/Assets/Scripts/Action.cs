using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class Action
{
    private string[] conditions_ = {"obstacle ahead", "!obstacle ahead", "hole ahead", "!hole ahead", "eagle ahead", "!eagle ahead", "destination reached", "!destination reached"};

    private string name_ = "";
    private int condition_id_ = -1;
    private int repeat_count_ = 1;
    private string command_ = "";
    private int command_repeat_count_ = 1;
    private string else_command_ = "";
    private int else_command_repeat_count_ = 1;

    public string GetName { get { return name_; } }
    public int GetRepeat { get { return repeat_count_; } }
    public int GetConditionId { get { return condition_id_; } }
    public string GetConditionText { get { return conditions_[condition_id_]; } }
    public int GetCommandRepeatCount { get { return command_repeat_count_; } }
    public string GetCommand { get { return command_; } }
    public string GetElseCommand { get { return else_command_; } }
    public int GetElseCommandRepeatCount { get { return else_command_repeat_count_; } }

    public string SetName { set { name_ = value; } }
    public int SetRepeat { set { repeat_count_ = value; } }
    public int SetConditionId { set { condition_id_ = value; } }
    public int SetCommandRepeatCount { set { command_repeat_count_ = value; } }
    public string SetCommand { set { command_ = value; } }
    public int SetElseCommandRepeatCount { set { else_command_repeat_count_ = value; } }
    public string SetElseCommand { set { else_command_ = value; } }

    // classic task building function for BT Scripts.
    private string BuildBasicTask(string lim, string time_before, string time_after)
    {
        return lim + AddRepeat(repeat_count_, 0) + lim + AddSequence(1) + lim + AddWait(time_before, 2) + lim + AddTask(name_.ToLower().Replace(" ", ""), 2) + lim + AddWait(time_after, 2);
    }

    private string BuildForTask(string lim, string time_before, string time_after)
    {
        return lim + AddRepeat(repeat_count_, 0) + lim + AddSequence(1) + lim + AddRepeat(command_repeat_count_, 2) + lim + AddSequence(3) + lim + AddWait(time_before, 4) + lim + AddTask(command_.ToLower().Replace(" ", ""), 4) + lim + AddWait(time_after, 4);
    }

    private string BuildIfTask(string lim, string[] offsets, string[] offsets_else)
    {
        string s = lim + AddSequence(0) + lim + AddRace(1) + lim + AddWhile(2) + lim + AddCondition(this.GetConditionText, 1, 3) + lim + AddRepeat(command_repeat_count_, 3)
         + lim + AddSequence(4) + lim + AddWait(offsets[0], 5) + lim + AddTask("loop_" +command_.ToLower().Replace(" ", ""), 5)
         + lim + AddWhile(2) + lim + AddElseCondition(this.GetConditionText, 3) + lim + AddRepeat(this.GetElseCommandRepeatCount, 3) + lim + AddSequence(4) + lim + 
          AddWait(offsets_else[0], 5);
        if (this.GetElseCommand != ""){
             s += lim + AddTask("loop_" + this.GetElseCommand.ToLower().Replace(" ", ""), 5);
        }
        return s;
    }
    
      private string BuildWhileTask(string lim, string[] offsets)
    {   
        return lim + AddMute(0) + lim + AddWhile(1) + lim + AddCondition(this.GetConditionText, 0, 2) + lim + AddRepeat(-1, 2) + lim + AddSequence(3) + lim + AddRepeat(command_repeat_count_, 4) 
        + lim + AddSequence(5) + lim + AddWait(offsets[0], 6) + lim + AddTask("loop_"+command_.ToLower().Replace(" ", ""), 6) + lim + AddWait(offsets[1], 6);
    }

    public string BuildAction(string lim){
        string source = "";
        if (name_ == "FOR"){
            string[] offsets = GetTimeOffsets(command_, false);
            source += BuildForTask(lim, offsets[0], offsets[1]);
        } 
        else if (name_ == "IF"){
            string[] offsets = GetTimeOffsets(command_, true);
            string[] offsets_else = GetTimeOffsets(else_command_, true);
            source += BuildIfTask(lim, offsets, offsets_else);
        }
        else if (name_ == "WHILE"){
            string[] offsets = GetTimeOffsets(command_, true);
            source = BuildWhileTask(lim, offsets);
        }
        else{
            string[] offsets = GetTimeOffsets(name_, false);
            source += BuildBasicTask(lim, offsets[0], offsets[1]);
        }
      
        return source;
    }

    private string[] GetTimeOffsets(string name, bool while_)
    {
        string[] offsets = {"0", "0"}; // in case the command space is empty - no time offsets
        if (!while_){
            if (name == "TURN LEFT" || name == "TURN RIGHT"){
                offsets[0] = "0.5";
                offsets[1] = "0.3";   
            }
            else if (name == "FORWARD" || name == "JUMP"){ 
                offsets[0] = "0.2";
                offsets[1] = "0.3";
            }
        }else if(name!=""){
            offsets[0] = "0.5";
        }
        if (name=="JUMP"){
            offsets[1] = "0.5";
        }
        return offsets;
    }

    /* ############## Add BT Script functions ############## */

    private string AddTask(string task_name, int tab)
    {
        return new String('\t', tab) + task_name;
    }

    private string AddWait(string time, int tab)
    {
        return new String('\t', tab) + "Wait " + time;
    }

    private string AddRepeat(int repeat, int tab)
    {
        string s = new String('\t', tab) + "repeat ";
        if (repeat >= 0){
            s += repeat;
        }
        return s;
    }

    private string AddSequence(int tab)
    {
        return new String('\t', tab) + "sequence";
    }

    private string AddRace(int tab)
    {
        return new String('\t', tab) + "race";
    }

    private string AddMute(int tab)
    {
        return new String('\t', tab) + "mute";
    }

    private string AddWhile(int tab)
    {
        string s = new String('\t', tab) + "while"; 
        return s;
    }

    private string AddCondition(string condition, int if_command, int tab)
    {
        return new String('\t', tab) + condition.Replace(" ", "_").Replace("!", "not ") + "(" + if_command + "," + Convert.ToInt32(condition.Contains("!")) + ")";
    }

    private string AddElseCondition(string condition, int tab)
    {
        string cond = "";
        if (condition.Contains("!")){
            cond = condition.Replace("!", "");
        }
        else{
            cond = "!" + condition;
        }
        return new String('\t', tab) + cond.Replace(" ", "_").Replace("!", "not ") + "(1," + Convert.ToInt32(cond.Contains("!")) + ")";
       
    }

    private string AddFallback(int tab)
    {
        return new String('\t', tab) + "fallback";
    }

}
