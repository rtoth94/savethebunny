using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Panda;


public class ActionController : MonoBehaviour
{
    public GameObject bunny;
    public bool StartExecution = false;
    public List<Action> ActionList { get { return action_list_; } }

    private List<Action> action_list_ = new List<Action>();
    public Tasks task_script;
    private GridBehaviour grid_behaviour;
    public PandaBehaviour PB;

    public bool close_panel = false;


    void Start()
    {
        task_script = bunny.GetComponent<Tasks>();
        grid_behaviour = bunny.GetComponent<GridBehaviour>();
        PB = bunny.GetComponent<PandaBehaviour>();
    }
    
    public void AddAction(Action act)
    {
        if(act.GetName != "" && act.GetRepeat > 0)
        {
            action_list_.Add(act);
        }
    }
    public void ExecuteActionQueue()
    {   
        string source = "tree(\"Root\")";
        string lim = "\n\t";

        if (action_list_.Count > 1)
        {
            source += "\n\tsequence";
            lim = "\n\t\t";
        }

        ClearTasks();

        foreach (Action action in action_list_)
        {
            source += action.BuildAction(lim);
            if(action.GetName == "FOR")
            {
                Action command_action = new Action();
                command_action.SetName = action.GetCommand;
                command_action.SetRepeat = action.GetCommandRepeatCount;
                for (int i = 0; i < action.GetRepeat; i++){
                    UpdateTasks(command_action);
                }
            }
            else{
                UpdateTasks(action);
            }
        }   
        if (action_list_.Count > 0){
             PB.Compile(source);
             StartExecution = true;
             Debug.Log(source);
        }
       
    }

    private bool ConditionMet(int conditionId)
    {
        
        return true;
    }

    public void ClearQueue()
    {
        action_list_.Clear();
    }

    public void ClearTasks()
    {
        task_script.N = 0;
        task_script.max = new List<int>();
        task_script.turn_count_left = 0;
        task_script.turn_count_right = 0;
        task_script.forwardActionId = 0;
        task_script.jump_count = 0;
        task_script.loops_to_run = 0;
    }

    public void Interrupt()
    {
        StartExecution = false;
        close_panel = true;
        PB.Reset();
    }

    void Update()
    {
        if(StartExecution)
        {
            if(task_script.isIdle()){
                Interrupt();
            } 

            PB.Tick();
        }
    }

    void UpdateTasks(Action action){
        if (action.GetName == "FORWARD"){
            task_script.max.Add(action.GetRepeat);
        }
        else if (action.GetName == "TURN LEFT"){
            task_script.turn_count_left++;
        }
        else if (action.GetName == "TURN RIGHT"){
            task_script.turn_count_right++;
        }
        else if (action.GetName == "JUMP"){
            task_script.jump_count++;
        }
        else if (action.GetName == "WHILE" || action.GetName == "IF"){
            task_script.loops_to_run++;
        }
    }

}