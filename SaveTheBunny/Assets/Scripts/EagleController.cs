using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EagleController : MonoBehaviour
{
    public GameObject bunny;
    public int EAGLE_ROTATION;
    public int RANGE = 4;
    private Animator animator;
    public float fly_speed = 1.0f;
    private bool can_catch = true;
    private Vector3 original_pos;
    private BunnyInteractionHandler bunny_script_;
    private Animator bunny_animator_;
    LifeHandler lfh_;

    private int rotation_status = 0;
    public GameObject step1_;

    void Start()
    {
        animator = this.GetComponent<Animator>();
        original_pos = this.transform.position;
        bunny_script_ = bunny.GetComponent<BunnyInteractionHandler>();
        bunny_animator_ = bunny.GetComponent<Animator>();
        lfh_ = Component.FindObjectOfType<LifeHandler>();
        step1_.GetComponent<Text>().text = EAGLE_ROTATION.ToString();
    }

    void Update()
    {
        bool go_catch = IsBunnyAhead();
        int bunny_distance = GetDistanceFromBunny();

        if (go_catch)
        {
            bunny_script_.CallMovementInterrupt();

            // Neet to catch but out of the catch range
            if (bunny_distance > 1)
            {
                //Debug.Log("FLY");
                this.transform.position = Vector3.MoveTowards(transform.position, bunny.transform.position, fly_speed * Time.deltaTime);
                animator.SetFloat("Speed", fly_speed);
            }
            if (bunny_distance < 2)
            {
                //Debug.Log("CATCH NOW");
                animator.SetFloat("Speed", 0);
                animator.SetTrigger("Attack");
                bunny_animator_.ResetTrigger("Revive");
                bunny_animator_.SetTrigger("Die");
            }
        }

        // Bunny is dead
        if (IsAnimationEnded(animator, "Attack") && IsAnimationEnded(bunny_animator_, "Dead"))
        {
            if (can_catch)
            {
                Debug.Log("Catch finished");
                ActionController action_controller = bunny.GetComponent<BunnyInteractionHandler>().action_controller;
                action_controller.Interrupt();
                animator.SetFloat("Speed", 0);
                animator.ResetTrigger("Attack");
                bunny_animator_.SetTrigger("Revive");
                lfh_.TakeLife();
                lfh_.ShowendScreen();

                Reset(true);
            }
            can_catch = false;


        }

    }

    public void Rotate()
    {
        rotation_status++;
        if (rotation_status > EAGLE_ROTATION)
        {
            rotation_status = 1;
        }

        if (rotation_status == EAGLE_ROTATION)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y + 90, transform.localEulerAngles.z);
        }
        else
        {
            Debug.Log("Rotation Status = " + rotation_status);
        }
    }

    private bool IsAnimationEnded(Animator anim, string animation_name)
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName(animation_name) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.8f)
        {
            return true;
        }
        can_catch = true;
        return false;
    }

    int GetDistanceFromBunny()
    {
        return (int)Mathf.Floor(Vector3.Distance(this.transform.position, bunny.transform.position));
    }

    bool IsBunnyAhead()
    {
        Vector3 RAY_START = transform.position + new Vector3(0, 0.5f, 0);
        Ray myRay = new Ray(RAY_START, transform.forward * RANGE);
        RaycastHit hit;

        if (Physics.Raycast(myRay, out hit, RANGE))
        {
            if (hit.collider.tag == "Bunny")
            {
                // Debug.Log("I See the bunny");
                return true;
            }
            else
            {
                // Debug.Log("Bunny Lost");
                animator.SetFloat("Speed", 0);
                return false;
            }
        }
        return false;
    }

    private void Reset(bool state)
    {
        bunny_animator_.ResetTrigger("Die");
        animator.ResetTrigger("Attack");
        animator.SetFloat("Speed", 0);
        bunny_script_.ResetBunny(state);
        this.transform.position = original_pos;
    }
}