﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeHandler : MonoBehaviour
{
    private int life_ = 1;

    GUI_Controller gui_;
    Inventory inventory_;
    BunnyInteractionHandler bunny_;


    void Update()
    {
        if(life_ < 0) 
        {
            life_ = 0;
        }
    }

    private void Start()
    {
        gui_ = Component.FindObjectOfType<GUI_Controller>();
        inventory_ = Component.FindObjectOfType<Inventory>();
        bunny_ = Component.FindObjectOfType<BunnyInteractionHandler>();
        
    }

    public void AddLife()
    {
        life_++;
    }

    public void TakeLife()
    {
        life_--;
        inventory_.UseCarrot();
    }

    public void ResetLifes() 
    {
        life_ = 1;
        inventory_.ResetSlots();
    }

    public int GetLifeCount()
    {
        return life_;
    }

    public bool ShowendScreen()
    {
        if (life_ <= 0)
        {
            gui_.FadeInImg(true);
            bunny_.ResetCarrots();
            
            return true;
        }
        else
        {
            gui_.FadeInImg(false);
            return false;
        }
    }
}
