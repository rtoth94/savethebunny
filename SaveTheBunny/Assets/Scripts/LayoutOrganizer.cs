﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Panda;
using System.Linq;

public class LayoutOrganizer : MonoBehaviour
{
    public GameObject element_panel;
    public GameObject bottom_panel;
   

    public GameObject target_panel_horizontal;
    public GameObject target_panel_vertical;
    public GameObject bunny_;

    private GameObject[] gridElems;
    private Animator panel_animator_;
    private ScrollRect scroll_rect_;
    
    bool watch_status = false;
    private PandaBehaviour panda_component;
    private Swipe swipe_controller;
    private ActionController action_controller;

    void Start()
    {
        gridElems = GameObject.FindGameObjectsWithTag("GridElement").OrderBy( go => go.name ).ToArray();
        panel_animator_ = GameObject.Find("SlidingPanel").GetComponent<Animator>();
        scroll_rect_ = GameObject.Find("ScrollPanelHorizontal").GetComponent<ScrollRect>();
        panda_component = bunny_.GetComponent<PandaBehaviour>();
        swipe_controller = this.transform.parent.GetComponent<Swipe>();
        action_controller = bunny_.GetComponent<BunnyInteractionHandler>().action_controller;
    }

    void Update()
    {
        if (action_controller.close_panel && watch_status) 
        {
            action_controller.close_panel = false;
            // Debug.Log("CLOSING PANEL");
            panel_animator_.SetTrigger("reset");
            watch_status = false;
        }
    }

    public void ResetLayout() 
    {
        HideOtherPanels(false);
        SetScrollPanelToVertical(false);
        ClearOccupied();
    }

    private void CopyOccupied() 
    {
       
        foreach (GameObject gridElem in gridElems)
        {
            if (gridElem.transform.childCount > 0)
            {
                gridElem.transform.SetParent(target_panel_vertical.transform);
                gridElem.transform.GetChild(0).transform.gameObject.GetComponent<CodeBlockHandler>().enabled = false;
            }
        }
    }

   private void ClearOccupied() 
    {
        Transform[] children = target_panel_vertical.transform.Cast<Transform>().ToArray();
        foreach(Transform child in children)
        {
            child.transform.SetParent(target_panel_horizontal.transform);
            int idx = int.Parse(child.name.Substring(child.name.Length -2)) -1;
            child.transform.SetSiblingIndex(idx);
        }
       
        foreach (GameObject gridElem in gridElems)
        {
            if (gridElem.transform.childCount > 0)
            {
                Destroy(gridElem.transform.GetChild(0).gameObject);
            }
        }
    }

    private void HideOtherPanels(bool state) 
    {
        element_panel.SetActive(!state);
        bottom_panel.SetActive(!state);
        target_panel_vertical.SetActive(state);
        target_panel_horizontal.SetActive(!state);
    }

    private void SetScrollPanelToVertical(bool state) 
    {
        scroll_rect_.horizontal = !state;
        scroll_rect_.vertical = state;

        if (state) 
        {
            scroll_rect_.content = target_panel_vertical.GetComponent<RectTransform>();
        }
        else 
        {
            scroll_rect_.content = target_panel_horizontal.GetComponent<RectTransform>();
        }
    }

    public void TriggerPanel() 
    {
        panel_animator_.SetTrigger("run");
        panel_animator_.ResetTrigger("reset");
        watch_status = true;
        swipe_controller.rt1_.ResetPos();
        swipe_controller.rt2_.ResetPos();
        HideOtherPanels(true);
        SetScrollPanelToVertical(true);
        CopyOccupied();
    }
}
