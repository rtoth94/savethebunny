﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetTarget : MonoBehaviour
{

    private Vector2 scrollbar_start_pos_;

    public Vector2 SetScrollbarRoot { set { scrollbar_start_pos_ = value; } }
    public Vector2 GetScrollbarRoot { get { return scrollbar_start_pos_; } }


    public void ResetPos() 
    {
        Vector2 target_pos = this.transform.gameObject.GetComponent<RectTransform>().anchoredPosition;
        if(target_pos.x < -25.0f) 
        {
            this.transform.gameObject.GetComponent<RectTransform>().anchoredPosition = scrollbar_start_pos_;
        }
    }
}
