﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelector : MonoBehaviour
{
    public GameObject[] levels;

    private int current_lvl_count = 0;
    private GameObject bunny_;
    private BunnyInteractionHandler bunny_handler_;
    public bool game_running = true;
    bool asd = true;

    private AudioController ac;

    void Start()
    {
        bunny_ = GameObject.FindGameObjectsWithTag("Bunny")[0];
        bunny_handler_ = bunny_.GetComponent<BunnyInteractionHandler>();
        ac = Component.FindObjectOfType<AudioController>();
        ac.PlayBackgroundMusic(current_lvl_count);
    }

    private void SwitchLevels(int lvl_index)
    {
        if(lvl_index <= levels.Length-1) 
        {
            levels[lvl_index - 1].gameObject.SetActive(false);
            levels[lvl_index].gameObject.SetActive(true);
            game_running = true;
        }
        bunny_handler_.CallEagleFinder();
        ac.PlayBackgroundMusic(lvl_index);
    }

    private void Update()
    {
        if (current_lvl_count < levels.Length)
        {
            asd = true;
            EndTrigger trigger = levels[current_lvl_count].GetComponentInChildren<EndTrigger>();
            if (trigger != null)
            {
                if (trigger.end_state && game_running)
                {
                    Debug.Log("GameEnded");
                    game_running = false;
                    TriggerNextLevel();
                }
            }
        }
        else if (current_lvl_count == levels.Length && asd) 
        {
            bunny_handler_.EndGame();
            asd = false;
        }
    }

    public void TriggerNextLevel()
    {
        current_lvl_count++;
        bunny_handler_.ResetBunny(true);
        bunny_handler_.ResetLifes();
        SwitchLevels(current_lvl_count);
    }
}
