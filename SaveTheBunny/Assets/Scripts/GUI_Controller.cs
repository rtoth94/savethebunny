using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class GUI_Controller : MonoBehaviour
{
    public Text step_label;
    public Text command_box;
    public Image fade;
    public Text gameOver;
    public Text final;
    public Text end_text;

    private Button increase_button, decrease_button, run_button;
    private List<string> m_DropOptions = new List<string> { "None", "Left", "Right"};
    private Dropdown m_Dropdown;
    private int step_count = 0;
    private bool add_jump = false;

    private ActionController action_controller;
    private List<string> printable_commands = new List<string>(0);

    private int highlighted_line_idx = 0;
    private int highlightedActions = 0;
    private GameObject[] gridElems;

    public bool img_visible = false;
    private EagleController eagle_controller;
    private EagleController[] eagleControllers;

    private LayoutOrganizer layoutOrganizer_;
    private BunnyInteractionHandler bunny_handler_;
    private BackToMenu menu_button_script;
    private bool is_end = false;

    void Start()
    {
        //m_Dropdown = GetComponentInChildren<Dropdown>();
        //m_Dropdown.ClearOptions();
        //m_Dropdown.AddOptions(m_DropOptions);
        printable_commands.Clear();
        step_label.GetComponent<Text>();

        fade = GameObject.FindWithTag("GameOver").GetComponent<Image>();
        fade.canvasRenderer.SetAlpha(0.0f);
        fade.enabled = false;
        gameOver = GameObject.Find("GameOverText").GetComponent<Text>();
        gameOver.enabled = false;
        final = GameObject.Find("FinalText").GetComponent<Text>();
        final.enabled = false;
        end_text = GameObject.Find("EndText").GetComponent<Text>();
        end_text.enabled = false;

        gridElems = GameObject.FindGameObjectsWithTag("GridElement").OrderBy( go => go.name ).ToArray();
        action_controller = GetComponent<ActionController>();

        layoutOrganizer_ = Component.FindObjectOfType<LayoutOrganizer>();
        bunny_handler_ = Component.FindObjectOfType<BunnyInteractionHandler>();
        menu_button_script = Component.FindObjectOfType<BackToMenu>();

        SetEagle();
    }

    void Update()
    {
    //    if(highlightedActions < action_controller.task_script.doneActions){
    //         UpdateList();
    //    }
       if(img_visible && Input.GetMouseButtonDown(0)){//  on click/touch anywhere the image will fade out
            FadeOutImg(false);
       }
       if(is_end && Input.GetMouseButtonDown(0)) 
       {
            FadeOutEnd();
       }
    }

    /* ################################# GUI Callbacks ################################# */

    public void OnIncreaseStep(GameObject btn)
    {
        Text countText = btn.transform.parent.Find("count").GetChild(0).GetComponent<Text>();
        countText.text = (int.Parse(countText.text) + 1).ToString();
    }

    public void SetEagle() 
    {
        eagleControllers = GameObject.FindObjectsOfType<EagleController>();
        foreach (EagleController ec in eagleControllers)
        {
            if (ec.isActiveAndEnabled)
            {
                eagle_controller = ec;
            }
        }
    }

    public void OnDecreaseStep(GameObject btn)
    {
        Text countText = btn.transform.parent.Find("count").GetChild(0).GetComponent<Text>();
        int current = int.Parse(countText.text);
        if (current > 0){
            countText.text = (current - 1).ToString();
        }
    }

    // public void OnAddJump(){
    //     m_Dropdown.value = 0;
    //     add_jump = true;
    // }

    public void OnClear()
    {
        action_controller.ClearQueue();
        action_controller.task_script.doneActions = 0;
    //     printable_commands.Clear();
    //     highlightedActions = 0;
    //     highlighted_line_idx = 0;
    //     add_jump = false;
    //     PrintCommands();
    }

    // public void OnAddAction()
    // {
    //     Action turn_action = GetTurnActionsFromGUI();
    //     Action move_action = GetMoveActionsFromGUI();

    //     // Add different actions
    //     if (!add_jump){
    //         action_controller.AddAction(turn_action);
    //         action_controller.AddAction(move_action);
    //     } else{
    //         Action jump_action = new Action();
    //         jump_action.SetName = "Jump";
    //         printable_commands.Add(jump_action.GetName);
    //         action_controller.AddAction(jump_action);
    //     }

    //     ResetGUI();
    //     PrintCommands();
    // }

    void ReadBlocks()
    {
        foreach (GameObject gridElem in gridElems)
        {
            if(gridElem.transform.childCount > 0){
                bool incomplete = false;
                Transform block = gridElem.transform.GetChild(0);
                Action action = new Action();
                action.SetName = block.name;
    
                Transform count = block.Find("count");
                if (count != null) // For or forward action
                {
                    action.SetRepeat = int.Parse(count.GetChild(0).GetComponent<Text>().text);
                }
                Transform dir = block.Find("direction");
                if (dir != null){ // Turn action
                    Dropdown dropdown = dir.GetComponent<Dropdown>();
                    action.SetName = action.GetName + " " + dropdown.options[dropdown.value].text.ToUpper();
                }
                Transform cond = block.Find("condition");
                if (cond != null){ // if or while action
                    Dropdown dropdown = cond.GetComponent<Dropdown>();
                    action.SetConditionId = dropdown.value;
                }
                Transform command = block.Find("COMMAND");
                if (command != null){
                    if(command.childCount != 0){
                        command = command.GetChild(0);
                        if (command.childCount > 1){ // forward command
                            action.SetCommand = "FORWARD";
                            action.SetCommandRepeatCount = int.Parse(command.GetChild(1).GetComponent<Text>().text);
                        } 
                        else { // jump or turn or if - then
                            action.SetCommand = command.GetChild(0).GetComponent<Text>().text.ToUpper();
                        }
                    }
                    else{
                        incomplete = true;
                    }
                    
                }
                Transform else_command = block.Find("ELSE_COMMAND");
                if (else_command != null){
                    if (else_command.childCount != 0){
                        else_command = else_command.GetChild(0);
                        if (else_command.childCount > 1){ // forward command
                            action.SetElseCommand = "FORWARD";
                            action.SetElseCommandRepeatCount = int.Parse(else_command.GetChild(1).GetComponent<Text>().text);
                        } 
                        else { // jump or turn
                            action.SetElseCommand = else_command.GetChild(0).GetComponent<Text>().text.ToUpper();
                        }
                    }
                    else  // if without else
                    {
                        action.SetElseCommand = "";
                    }
                }
                if (!incomplete){
                    action_controller.AddAction(action);
                } 
            }
        }
    }

    public void OnRun()
    {   
        ReadBlocks();
         if(action_controller.ActionList.Count > 0){
            layoutOrganizer_.TriggerPanel();
            if(eagle_controller != null) 
            {
                eagle_controller.Rotate();       
            }
        }
        bunny_handler_.SaveCurrentPos();
     
        // ResetGUI();
        // PrintCommands();
        // highlighted_line_idx = command_box.text.IndexOf("\n", highlighted_line_idx+1);
    }

    public void TriggerRun() 
    {
        // ReadBlocks();
        action_controller.ExecuteActionQueue();
        action_controller.ClearQueue();
    }

    /* ################################# Action from GUI ################################# */

    Action GetTurnActionsFromGUI()
    {
        Action turn_act = new Action();
        if(m_Dropdown.value == 1)
        {   
            turn_act.SetName = "Turn Left";
            printable_commands.Add(turn_act.GetName);
            add_jump = false;
        }
        if(m_Dropdown.value == 2)
        {
            turn_act.SetName = "Turn Right";
            printable_commands.Add(turn_act.GetName);
            add_jump = false;
        }
        return turn_act;
    }

    Action GetMoveActionsFromGUI()
    {
        Action move_act = new Action();
        move_act.SetRepeat = step_count;
        move_act.SetName = "Forward";

        if(step_count > 0)
        {
            printable_commands.Add("Forward " + step_count.ToString());
        }
        return move_act;
    }

        /* ################################# Utilities ################################# */

    void PrintCommands()
    {
        command_box.text = "";
        if (highlightedActions>0){
            command_box.text += "<b>";
        }
        int idx = 0;
        //Debug.Log(printable_commands.Count);
        foreach (string action in printable_commands)
        {
            idx++;
            if (highlightedActions==idx){
                command_box.text += action + "</b>\n";
            }
            else{
                command_box.text += action + "\n";
            }
        }
        
    }

    public void FadeInImg(bool is_final_panel)
    {
        fade.enabled = true;
        if (is_final_panel) 
        {
            final.enabled = true;
            final.canvasRenderer.SetAlpha(0.0f);
            fade.canvasRenderer.SetAlpha(0.0f);
            fade.CrossFadeAlpha(1.0f, 0.5f, false);
            final.CrossFadeAlpha(1.0f, 0.5f, false);
            img_visible = true;
        }
        else 
        {
            gameOver.enabled = true;
            gameOver.canvasRenderer.SetAlpha(0.0f);
            fade.canvasRenderer.SetAlpha(0.0f);
            fade.CrossFadeAlpha(1.0f, 0.5f, false);
            gameOver.CrossFadeAlpha(1.0f, 0.5f, false);
            img_visible = true;
        }
    
    }

    public void FadeOutImg(bool is_final)
    {
        fade.CrossFadeAlpha(0.0f, 0.5f, false);
        img_visible = false;
        gameOver.enabled = false;
        fade.enabled = false;
        if(final.enabled) 
        {
            final.enabled = false;
            menu_button_script.LaunchStartMenu();
        }
    }

    public void FadeInEnd()
    {
        fade.enabled = true;
        end_text.enabled = true;
        end_text.canvasRenderer.SetAlpha(0.0f);
        fade.canvasRenderer.SetAlpha(0.0f);
        fade.CrossFadeAlpha(1.0f, 0.5f, false);
        end_text.CrossFadeAlpha(1.0f, 0.5f, false);
        img_visible = true;
        is_end = true;
    }

    public void FadeOutEnd()
    {
        fade.CrossFadeAlpha(0.0f, 0.5f, false);
        img_visible = false;
        gameOver.enabled = false;
        fade.enabled = false;

        is_end = false;
        menu_button_script.LaunchStartMenu();
    }

    void ResetGUI()
    {
        foreach(GameObject gridElem in gridElems)
        {
            if (gridElem.transform.childCount > 0){
                Destroy(gridElem.transform.GetChild(0).gameObject);
            }
        }
    }

    public void UpdateList() // visualize already executed actions on the run
    {
        // find next occurence of new line
        highlighted_line_idx = find_all(command_box.text, "\n")[highlightedActions];
        // find the end of highlighted text
        int end_bold = command_box.text.IndexOf("</b>");
        if (end_bold < 0){
            end_bold = 0;
            command_box.text = "<b>" + command_box.text.Substring(0,highlighted_line_idx) + "</b>" + command_box.text.Substring(highlighted_line_idx,command_box.text.Length-highlighted_line_idx);
        }
        else{
            command_box.text = command_box.text.Substring(0, end_bold) + command_box.text.Substring(end_bold+4,highlighted_line_idx - (end_bold+4)) + "</b>" + command_box.text.Substring(highlighted_line_idx,command_box.text.Length-(highlighted_line_idx));
        }
        highlightedActions++;
    }

    private List<int> find_all(string text, string element){
        List<int> indices = new List<int>();
        for (int index = 0;; index += element.Length) {
        index = text.IndexOf(element, index);
        if (index == -1)
            return indices;
        indices.Add(index);
        }
    }

    public void OnRunFinished() 
    {
        layoutOrganizer_.ResetLayout();
    }
}
