﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class Swipe : MonoBehaviour
{
    private float deadzone = 50.0f;
    private float double_tap_delta = 0.5f;
    private bool tap, double_tap, swipe_left, swipe_right, swipe_up, swipe_down, clicked_on_panel;
    private Vector2 swipe_delta, touch_start;
    private float last_tap;
    private float sqr_deadzone;
    private Animator side_animator;

    public ResetTarget rt1_;
    public ResetTarget rt2_;

    void Start()
    {
        sqr_deadzone = deadzone * deadzone;
        side_animator = this.GetComponent<Animator>();
        clicked_on_panel = false;

        GameObject scroll1 = GameObject.Find("Target");
        GameObject scroll2 = GameObject.Find("Content");

        rt1_ = scroll1.GetComponent<ResetTarget>();
        rt1_.SetScrollbarRoot = scroll1.transform.position;

        rt2_ = scroll2.GetComponent<ResetTarget>();
        rt2_.SetScrollbarRoot = scroll2.transform.position;
    }

    void Update()
    {
        UnityControls();
    }

    public void OnPanelDrag() 
    {
         //Debug.Log("Panel Dragged");
         clicked_on_panel = true;
    }

    public void OnPanelDragEnd() 
    {
         //Debug.Log("Panel Drag ended");
         clicked_on_panel = false;
    }

    private void UnityControls()
    {
        if(Input.GetMouseButtonDown(0))
        {
            tap = true;
            touch_start = Input.mousePosition;
            double_tap = Time.time - last_tap < double_tap_delta;
            last_tap = Time.time;
            //Debug.Log("Tap");
        }
        else if(Input.GetMouseButtonUp(0))
        {
            touch_start = swipe_delta = Vector2.zero;
            //Debug.Log("Reset");
        }

        // Calculate swipe vector
        swipe_delta = Vector2.zero;
        bool touch_started_with_left_click = (touch_start != Vector2.zero && Input.GetMouseButton(0));
        if(touch_started_with_left_click && clicked_on_panel)
        {
            swipe_delta = (Vector2)Input.mousePosition - touch_start;
        }

        // Detect if swipe is larger then deadzone
        if(swipe_delta.sqrMagnitude > sqr_deadzone)
        {
            float x = swipe_delta.x;
            float y = swipe_delta.y;

            if(Mathf.Abs(x) > Mathf.Abs(y))
            {
                if(x < 0)
                {
                    swipe_left = true;
                    //Debug.Log("Swipe Left");
                    side_animator.SetTrigger("open_panel");
                }
                else
                {
                    swipe_right = true;
                    //Debug.Log("Swipe right");
                    side_animator.SetTrigger("close_panel");
                    rt1_.ResetPos();
                    rt2_.ResetPos();
                }
            }
            else
            {
                if(y < 0)
                {
                    swipe_down = true;
                    //Debug.Log("Swipe down");
                }
                else
                {
                    swipe_up = true;
                    //Debug.Log("Swipe up");
                }
            }

            touch_start = swipe_delta = Vector2.zero;
        }
    }


    private void MobileControls() 
    {

    }

}
