﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunnyInteractionHandler : MonoBehaviour
{
    public ActionController action_controller;
    private Rigidbody RB;
    private Collider bunny_colider;
    private Vector3 original_position;
    private static string HOLE = "Hole";
    private static string WALL = "Wall";
    private static string COLLECTIBLE = "Collectible";
    private static string CARROT = "CARROT";
    private static float THRESHOLD_UNDER_MAP = -1.0f;
    private GridBehaviour movement_script_;
    private Animator animator;
    private GUI_Controller gui;
    private Inventory inventory;
    private Tasks tasks;
    private Vector3 position_from_run_;
    public List<GameObject> collected_carrots_;
    private LifeHandler lfh_;
    private AudioController ac;

    void Start()
    {
        movement_script_ = this.GetComponent<GridBehaviour>();
        tasks = this.GetComponent<Tasks>();
        RB = this.GetComponent<Rigidbody>();
        bunny_colider = this.GetComponent<Collider>();
        original_position = this.transform.position;
        animator = this.GetComponent<Animator>();
        gui = Component.FindObjectOfType<GUI_Controller>();
        inventory = Component.FindObjectOfType<Inventory>();
        lfh_ = Component.FindObjectOfType<LifeHandler>();
        ac = Component.FindObjectOfType<AudioController>();
        ResetBunnyPhysics();
    }

    public void SaveCurrentPos() 
    {
        position_from_run_ = this.transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == HOLE)
        {
            bunny_colider.isTrigger = true;
            RB.isKinematic = false;
            action_controller.Interrupt();
        }
        if(other.tag == COLLECTIBLE)
        {
            PickUp(other);
        }
        if (other.tag == CARROT)
        {
            PickUp(other);
        }
    }

    public void CallMovementInterrupt()
    {
        action_controller.Interrupt();
    }

    private void OnTriggerStay(Collider collectible)
    {
        if (collectible.tag == COLLECTIBLE){
            EndPickUpSequence(collectible);
        }
    }

    private void OnTriggerExit(Collider collectible)
    {
        if (collectible.tag == COLLECTIBLE){
            EndPickUpSequence(collectible);
        }
    }

    private void EndPickUpSequence(Collider collectible)
    {
        Animator local_animator = collectible.transform.parent.gameObject.GetComponent<Animator>();
        if(IsAnimationEnded(local_animator, "PickUp"))
            {
                inventory.AddItemToSlot(collectible.gameObject);
                local_animator.ResetTrigger("Pick"); 
                
                collected_carrots_.Add(collectible.gameObject);
                collectible.gameObject.SetActive(false);

                lfh_.AddLife();
            }
    }

    private void PickUp(Collider collectible)
    {
        Animator local_animator = collectible.transform.parent.gameObject.GetComponent<Animator>();
        local_animator.SetTrigger("Pick");
        ac.PlayEatSound();
    }

    void Update()
    {   
        // If bunny fall into hole, reset position and physics
        if(this.transform.position.y < THRESHOLD_UNDER_MAP)
        {
            ac.PlayFallSound();
            bool tmp = TakeLife();
            ResetBunny(tmp);
        }

        // Controlled in movement script
        if(movement_script_.obstacle_ahead)
        {
            SetMaterialTransparent();
            movement_script_.obstacle_ahead = false;
            ac.PlayFailedSound();
            action_controller.Interrupt();
        }
    }

    private bool TakeLife() 
    {
        lfh_.TakeLife();
        bool end_state = lfh_.ShowendScreen();
        return end_state;
    }

    // Trigger from Fade Animation
    public void FadeEnded() 
    {
        bool tmp = TakeLife();
        movement_script_.obstacle_ahead = false;
        ResetBunny(tmp);
        SetMaterialOpaque();
    }

    private bool IsAnimationEnded(Animator anim, string animation_name)
    {
        if(anim.GetCurrentAnimatorStateInfo(0).IsName(animation_name) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.8f)
        {
            return true;
        }
        return false;
    }

    //Source: https://www.patreon.com/posts/c-script-to-hide-7678022   BLACK MAGIC
    private void SetMaterialTransparent()
    {
        foreach (Renderer r in this.GetComponentsInChildren<Renderer>())
        {
            foreach (Material m in r.materials)
        {
            m.SetFloat("_Mode", 2);
            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            m.SetInt("_ZWrite", 0);
            m.DisableKeyword("_ALPHATEST_ON");
            m.EnableKeyword("_ALPHABLEND_ON");
            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            m.renderQueue = 3000;
            }
        }
        animator.SetBool("FadeOut", true);
    }

    private void SetMaterialOpaque()
    {
        foreach (Renderer r in this.GetComponentsInChildren<Renderer>())
        {
            foreach (Material m in r.materials)
            {
            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
            m.SetInt("_ZWrite", 1);
            m.DisableKeyword("_ALPHATEST_ON");
            m.DisableKeyword("_ALPHABLEND_ON");
            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            m.renderQueue = -1;
            }
        }
        animator.SetBool("FadeOut", false);
    }

    public void ResetBunny(bool reset_to_start)
    {
        ResetBunnyPhysics();
        if (reset_to_start) 
        {
            this.transform.position = original_position;
        }
        else 
        {
            this.transform.position = position_from_run_;
        }
            
        tasks.loops_to_run = 0;
        movement_script_.ResetPos();
        gui.OnClear();
    }

    private void ResetBunnyPhysics()
    {
        bunny_colider.isTrigger = false;
        RB.isKinematic = true;
    }

    public void ResetLifes() 
    {
        lfh_.ResetLifes();
    }

    public void ResetCarrots() 
    {
        if(collected_carrots_.Count > 0) 
        {
            foreach(GameObject carr in collected_carrots_)
            {
                if (!carr.activeSelf) 
                {
                    carr.SetActive(true);
                }
            }
        }
    }

    public void CallEagleFinder() 
    {
        gui.SetEagle();
    }

    public void EndGame() 
    {
        gui.FadeInEnd();
    }
}