﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTrigger : MonoBehaviour
{
    public bool end_state = false;

    public void GameEnded() 
    {
       end_state = true;
    }
}
