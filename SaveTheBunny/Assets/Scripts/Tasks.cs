﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Panda;

public class Tasks : MonoBehaviour
{
    public int N = 0; // internal counter
    public List<int> max =  new List<int>(1); // max step count
    public int loops_to_run = 0;
    public int forwardActionId = 0;
    public int turn_count_left = 0;
    public int turn_count_right = 0;
    public int jump_count = 0;
    public int doneActions = 0;
    private bool if_loop = false;

    [Task]
    public void forward()
    {
        if (N < max[forwardActionId]){
            this.GetComponent<GridBehaviour>().go_forward = true;
            N++;
            Task.current.Succeed();
            if (N==max[forwardActionId]){
                if (max.Count >= (forwardActionId+2)){
                    forwardActionId++;
                    N = 0;
                }
                doneActions++;
            }
        }
        else{
            Task.current.Fail();      
        }
        
        
    }

    [Task]
    public bool isIdle()
    {
        if (max.Count==0){
            max.Add(0);
        }
        return !CanMove() && !CanTurn() && jump_count==0 && loops_to_run==0;
    }

    [Task]
    bool CanMove()
    {
        return (N < max[forwardActionId]) && (forwardActionId <= max.Count-1);
    }

    [Task]
    bool CanTurn()
    {
        return (turn_count_left>0 || turn_count_right>0);
    }

    [Task]
    void turnleft()
    {   
        if(turn_count_left > 0)
        {
            this.GetComponent<GridBehaviour>().TurnLeft();
            turn_count_left--;
            doneActions++;
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    void turnright()
    {   
        if(turn_count_right > 0)
        {
            this.GetComponent<GridBehaviour>().TurnRight();
            turn_count_right--;
            doneActions++;
            Task.current.Succeed();
        }
        else
        { 
            Task.current.Fail();
        }
    }

    [Task]
    public void jump()
    {
        if (jump_count>0){
            this.GetComponent<GridBehaviour>().jump = true;
            doneActions++;
            jump_count--;
            Task.current.Succeed();
        }
        else{
            Task.current.Fail();
        }
       
    }

    [Task]
    public void loop_forward()
    {
        this.GetComponent<GridBehaviour>().go_forward = true;
        Task.current.Succeed();
        if(if_loop){
            loops_to_run--;
        }
    }

    [Task]
    public void loop_turnleft()
    {
        this.GetComponent<GridBehaviour>().TurnLeft();
        Task.current.Succeed();
        if(if_loop){
            loops_to_run--;
        }
    }

    
    [Task]
    public void loop_turnright()
    {
        this.GetComponent<GridBehaviour>().TurnRight();
        Task.current.Succeed();
        if(if_loop){
            loops_to_run--;
        }
    }

    [Task]
    public void loop_jump()
    {
        this.GetComponent<GridBehaviour>().jump = true;
        Task.current.Succeed();
        if(if_loop){
            loops_to_run--;
        }
    }


    [Task]
    bool obstacle_ahead(int if_command, int not){// TODO remove if_command
        if_loop = if_command==1 ? true : false;
        if(this.GetComponent<GridBehaviour>().IsObstacleAhead(true)){
            if(not==1 && if_command==0){
                loops_to_run--;
            }
            return true;
        }
    
        if (not==0 && if_command==0){
            loops_to_run--; // end of cycle
        }
        return false;
    }

    [Task]
    bool hole_ahead(int if_command, int not){
        if_loop = if_command==1 ? true : false;
        if(this.GetComponent<GridBehaviour>().IsHoleAhead()){
            if(not==1 && if_command==0){
                loops_to_run--;
            }
            return true;
        }
    
        if (not==0 && if_command==0){
            loops_to_run--; // end of cycle
        }

        return false;
    }


    [Task]
    bool eagle_ahead(int if_command, int not){
        if_loop = if_command==1 ? true : false;
        if(this.GetComponent<GridBehaviour>().IsEagleAhead()){
            if(not==1 && if_command==0){
                loops_to_run--;
            }
            return true;
        }
     
      
        if (not==0 && if_command==0){
            loops_to_run--; // end of cycle
        }

        return false;
    }

    [Task]
    bool destination_reached(int if_command, int not){
        if_loop = if_command==1 ? true : false;
         if(this.GetComponent<GridBehaviour>().IsDestinationReached()){
            if(not==1 && if_command==0){
                loops_to_run--;
            }
            return true;
        }
     
      
        if (not==0 && if_command==0){
            loops_to_run--; // end of cycle
        }

        return false;
    }
}