﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CodeBlockHandler : MonoBehaviour, IDragHandler, IEndDragHandler
{
    private static string ELEMENT_BLOCK = "ElementBlock";
    private static string CODE_BLOCK = "CodeBlock";
    private RectTransform target_;
    private RectTransform bin_;
    private Vector2 stored_position_;

    private GraphicRaycaster m_Raycaster;
    private PointerEventData m_PointerEventData;
    private EventSystem m_EventSystem;
    private GameObject grid_;

    public Vector2 SetPlacedPosition { set { stored_position_ = value; } }

    void Start()
    {
        target_ = GameObject.Find("Target").GetComponent<RectTransform>();
        bin_ = GameObject.Find("Bin").GetComponent<RectTransform>();
        m_Raycaster = GameObject.Find("Canvas").GetComponent<GraphicRaycaster>();
        m_EventSystem = GetComponent<EventSystem>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = GetCurrentMousePos();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!isInTarget()) 
        {
            this.transform.position = stored_position_;
            Cast();
            if (grid_ != null && grid_.gameObject.tag == "Bin")
            {
                Destroy(this.transform.gameObject);
            }
        }
        else 
        {
            Cast();
            if ((grid_ != null && !HasChild(grid_)) && this.transform.gameObject.tag != "ElementaryAction")
            {
                SetCenterAnchor(this.transform.gameObject);
                this.transform.SetParent(grid_.transform);
                this.transform.gameObject.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
                this.transform.gameObject.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
                stored_position_ = this.transform.position;
            }
            else 
            {
                this.transform.position = stored_position_;
            }
        }
    }

    private Vector2 GetCurrentMousePos()
    {
        return new Vector2(Input.mousePosition.x, Input.mousePosition.y);
    }

    public void Cast()
    {
        m_PointerEventData = new PointerEventData(m_EventSystem);
        m_PointerEventData.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        m_Raycaster.Raycast(m_PointerEventData, results);

        foreach (RaycastResult result in results)
        {
            if (result.gameObject.tag == "GridElement" || result.gameObject.tag == "Bin")
            {
                grid_ = result.gameObject;
            }
        }
    }

    private bool HasChild(GameObject obj)
    {
        return obj.transform.childCount > 0;
    }

    private void SetCenterAnchor(GameObject go)
    {
        go.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
        go.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
        go.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
    }

    private bool isInTarget()
    {
        return RectTransformUtility.RectangleContainsScreenPoint(target_, Input.mousePosition);
    }
}
