﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBehaviour : MonoBehaviour
{
    public bool go_forward = false;
    public bool obstacle_ahead = false;
    public bool jump = false;
    public bool isJumping = false;
    private bool canMove;
    private Animator m_Animator;
    private ActionController m_ActionController;
    private Vector3 up = Vector3.zero, 
    right = new Vector3(0, 90, 0),
    down = new Vector3(0, 180, 0),
    left = new Vector3(0, 270, 0),
    currentDir = Vector3.zero;
    Vector3 nextPos, destination, direction;
    private float speed = 2.4f;
    private float rayLength = 1f;
    private Collider hole;

    private AudioController ac;

    void Start()
    {
        currentDir = up;
        nextPos = Vector3.forward;
        destination = transform.position;
        m_Animator = gameObject.GetComponent<Animator>();
        m_ActionController = gameObject.GetComponent<BunnyInteractionHandler>().action_controller;
        ac = Component.FindObjectOfType<AudioController>();
    }

    private void LateUpdate()
    {
      Move();   
    }

    public void TurnRight()
    { 
        if(currentDir == left)
        {
            currentDir = up;
        }
        else
        {
            currentDir.y += 90;
        }
        canMove = false;
        transform.localEulerAngles = currentDir;
    }

    public void TurnLeft()
    { 
        if(currentDir == up)
        {
            currentDir = left;
        }
        else
        {
            currentDir.y -= 90;
        }
        canMove = false;
        transform.localEulerAngles = currentDir;
        
    }

    void Move()
    {
        m_Animator.ResetTrigger("IsMoving");
        m_Animator.ResetTrigger("IsJumping");
        transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);
        if(go_forward || jump)
        {
     
            if (hole!=null){
                if(hole.GetComponent<SphereCollider>() != null){
                    hole.GetComponent<SphereCollider>().isTrigger = true;
                }else{
                    hole.GetComponent<BoxCollider>().isTrigger = true;
                }
            }
            canMove = true;
            go_forward = false;
            
            if(jump){
                isJumping = true;
                nextPos = transform.forward * 2;
            }else{
                rayLength = 1;
                nextPos = transform.forward;
            }
            jump = false;
        }

        if(Vector3.Distance(destination, transform.position) <= 0.0001f)
        {
          
            // This is the reason why it rotates at start, but if you move inside the if, the first rotate command will be ignored o.O
            transform.localEulerAngles = currentDir;

            if(canMove)
            {   
                if ((isJumping) && !IsObstacleAhead(false) && !IsObstacleOnJump()){
                    hole = DeactivateHoleAhead(); // and carrot
                    m_Animator.SetTrigger("IsJumping");
                    destination = transform.position + nextPos;
                    direction = nextPos;                
                    canMove = false;
                    if(IsDestinationAhead(true)){
                        m_ActionController.Interrupt();
                    }
                }
                else if (!IsObstacleAhead(false) && !isJumping){
                    m_Animator.SetTrigger("IsMoving");
                    ac.PlayMoveSound();
                    destination = transform.position + nextPos;
                    direction = nextPos;                
                    canMove = false;
                }
                if(IsDestinationAhead(false)){
                    m_ActionController.Interrupt();
                }
            }
            isJumping = false;
        }
    }

    bool IsObstacleOnJump(){
        Ray myRay = new Ray(transform.position + transform.forward + new Vector3(0, 0.25f, 0), nextPos);
        RaycastHit hit;

        Debug.DrawRay(myRay.origin, myRay.direction, Color.red);

        if(Physics.Raycast(myRay, out hit, rayLength))
        {
            if(hit.collider.tag == "Wall" || hit.collider.tag == "Box" || hit.collider.tag == "Bush")
            {
                Debug.Log("I see obstacle on Jump");
                obstacle_ahead = true;
                canMove = false;
                return true;
            }
        }
        return false;
    }

    Collider DeactivateHoleAhead(){ // hole or carrot
        Ray myRay = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        Debug.DrawRay(myRay.origin, myRay.direction, Color.red);
        if(Physics.Raycast(myRay, out hit, rayLength))
        {
            if(hit.collider.tag == "Hole" || hit.collider.tag == "Collectible")
            {
                Debug.Log("I see hole or carrot");
                if(hit.collider.GetComponent<SphereCollider>() != null){ 
                    hit.collider.GetComponent<SphereCollider>().isTrigger = false;
                }
                else{
                    hit.collider.GetComponent<BoxCollider>().isTrigger = false;
                }
                return hit.collider;
            }
        }
        return null;
  
    }

    public bool IsDestinationReached()
    {
        Ray myRay = new Ray(transform.position + new Vector3(0, 0.25f, 0), transform.up);
        RaycastHit hit;
     
        Debug.DrawRay(myRay.origin, myRay.direction, Color.red);
        if(Physics.Raycast(myRay, out hit, rayLength))
        {
            if(hit.collider.tag == "CARROT")
            {
                return true;
            }
        }
        return false;
    }

     public bool IsDestinationAhead(bool jumping)
    {
        Ray myRay;
        if(jumping){
            myRay = new Ray(transform.position + transform.forward + new Vector3(0, 0.25f, 0), nextPos);
        }
        else {
            myRay = new Ray(transform.position + new Vector3(0, 0.25f, 0), transform.forward);
        }
        
        RaycastHit hit;
     
        Debug.DrawRay(myRay.origin, myRay.direction, Color.red);
        if(Physics.Raycast(myRay, out hit, rayLength))
        {
            if(hit.collider.tag == "CARROT")
            {
                return true;
            }
        }
        return false;
    }

    public bool IsObstacleAhead(bool just_check)
    {
        Ray myRay = new Ray(transform.position + new Vector3(0, 0.25f, 0), transform.forward);
        RaycastHit hit;
        Debug.DrawRay(myRay.origin, myRay.direction, Color.red);
        if(Physics.Raycast(myRay, out hit, rayLength))
        {
            if(hit.collider.tag == "Wall" || hit.collider.tag == "Box" || hit.collider.tag == "Bush")
            {
                // Debug.Log("I see obstacle");
                if (!just_check)
                {
                    obstacle_ahead = true;
                }
                canMove = false;
                return true;
            }
        }
        return false;
    }

    public bool IsHoleAhead()
    {
        Ray myRay = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        Debug.DrawRay(myRay.origin, myRay.direction, Color.red);

        if(Physics.Raycast(myRay, out hit, rayLength))
        {
            if(hit.collider.tag == "Hole")
            {
                canMove = false;
                return true;
            }
        }
        return false;
    }

    public bool IsEagleAhead()
    {
        Ray myRay = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        Debug.DrawRay(myRay.origin, myRay.direction, Color.red);

        if(Physics.Raycast(myRay, out hit, rayLength))
        {
            if(hit.collider.tag == "Ranger")
            {
                canMove = false;
                return true;
            }
        }
        return false;
    }


    public void ResetPos()
    {
        destination = transform.position;
        currentDir = up;
    }

    private void LegacyControls()
    {
         if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            nextPos = -transform.forward;
            canMove = true;
        }
        if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            if(currentDir == left)
            {
                currentDir = up;
            }
            else
            {
                currentDir.y += 90;
            }
            canMove = false;
        }
        if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if(currentDir == up)
            {
                currentDir = left;
            }
            else
            {
                currentDir.y -= 90;
            }
            canMove = false;
        }
    }
}
