﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ElementCloner : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{

  /*
   * This script handles the Drag n Drop events from the Element Panel. 
   * After the copied element is placed down, a new script CodeBlockHandler handles the rest.
   */

    public float element_holding_time = 1.0f;
    private static string CODE_BLOCK = "CodeBlock";
    private static string COMMAND_TAG = "Command";
    private static string ELEMENTARY_ACTION = "ElementaryAction";
    private Dictionary<int, string> dropdown_options = new Dictionary<int, string>();

    private RectTransform target_area;
    private GameObject clone_element_;
    private CodeBlockHandler clone_script_;
  
    private bool start_timer = false;
    private float click_time = 0.0f;
    private bool can_copy = false;

    private GraphicRaycaster m_Raycaster;
    private PointerEventData m_PointerEventData;
    private EventSystem m_EventSystem;

    private GameObject grid_;
    private GameObject command_slot;
    public GameObject command_prefab;

    void Start()
    {
        target_area = GameObject.Find("Target").GetComponent<RectTransform>();
        clone_element_ = null;

        m_Raycaster = GameObject.Find("Canvas").GetComponent<GraphicRaycaster>(); 
        m_EventSystem = GetComponent<EventSystem>();

        dropdown_options.Add(0, "Turn left");
        dropdown_options.Add(1, "Turn right");
  }

    void Update()
    {
        if (start_timer)
        {
            float elapsed_time_after_click = Time.time - click_time;
            bool hold_enough = elapsed_time_after_click >= element_holding_time;
            if (hold_enough && can_copy)
            {
                CreateClone();
                can_copy = false;
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        click_time = Time.time;
        start_timer = true;
        can_copy = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        start_timer = false;
        can_copy = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isCloneExists())
        {
            clone_element_.transform.position = GetCurrentMousePos();
            DetectGUIElementsUnderCursor();
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        // If we want to release the copied object in target area.
        if (isInTarget() && isCloneExists())
        {   
            // If you want to put into a grid element
            if (!HasChild(grid_))
            {
              DropIntoActionList();
            }
            // Or some elements into a command slot
            else if (isCommandSlotExists() && DraggingElementaryAction())
            {   
              DropIntoCommandSlot(command_prefab);
            }
            else
            {
                Destroy(clone_element_);
            }
        }
        Destroy(clone_element_);
    }

    /* #################################################################################### */

    private void DropIntoCommandSlot(GameObject prefab) 
    {
      CreateCommandClone(prefab);
      PlaceIntoGameObject(clone_element_, command_slot);
    }

    private void DropIntoActionList() 
    {
      PlaceIntoGameObject(clone_element_, grid_);
      clone_element_ = null;
    }

    private void PlaceIntoGameObject(GameObject obj, GameObject target) 
    {
    /*
     * function takes an object and makes it child of target game object.  Sets the anchor, and pivot position to center. 
     * If Target is occupied destroys the object
     */
      if(target.transform.childCount == 0) 
      {
        SetCenterAnchor(obj);
        obj.transform.SetParent(null);
        obj.transform.SetParent(target.transform);
        Debug.Log("setting parent to " + target);
        obj.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        obj.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
        clone_script_.SetPlacedPosition = obj.transform.position;
        clone_element_ = null;
      }
      else 
      {
        Destroy(obj);
      }
  }

    private void CreateClone() 
    {
        /*
         * This function creates a clone object (CodeBlock) from original Element Block. Assigns scripts and destroys unnecesarry elements
         */
        clone_element_ = Instantiate(this.transform.gameObject, this.transform.position + new Vector3(25.0f,25.0f, 0.0f), Quaternion.identity, target_area.transform);
        clone_element_.gameObject.tag = CODE_BLOCK;
        clone_element_.gameObject.name = this.gameObject.name;

        // Set the size according the parent
        Vector2 original_size = this.transform.gameObject.GetComponent<RectTransform>().sizeDelta;
        clone_element_.GetComponent<RectTransform>().sizeDelta = original_size;

        Destroy(clone_element_.gameObject.GetComponent<ElementCloner>());
        Destroy(clone_element_.gameObject.GetComponent<EventTrigger>());

        // Initializes the position of the new object
        clone_script_ = clone_element_.AddComponent<CodeBlockHandler>();
    }

    private void CreateCommandClone(GameObject command_prefab) 
    {   
        Destroy(clone_element_);
        clone_element_ = Instantiate(command_prefab.transform.gameObject, this.transform.position + new Vector3(25.0f, 25.0f, 0.0f), Quaternion.identity, target_area.transform);
        clone_element_.gameObject.tag = ELEMENTARY_ACTION;
        
        // Here we resize the FORWARD and the TURN blocks according to the slot where we put it.
        if(command_prefab.gameObject.name == "FORWARD_COMMAND") 
        {
          string data = this.transform.GetChild(3).GetComponentInChildren<Text>().text;
          SetForwardData(data);
        }
        if (command_prefab.gameObject.name == "TURN_COMMAND")
        {
          int data_index = this.transform.GetChild(1).GetComponentInChildren<Dropdown>().value;
          SetTurnData(dropdown_options[data_index]);
        }

        Vector2 original_size = command_slot.gameObject.GetComponent<RectTransform>().sizeDelta;
        clone_element_.GetComponent<RectTransform>().sizeDelta = original_size;

        clone_element_.gameObject.name = command_prefab.gameObject.name;
        clone_script_ = clone_element_.AddComponent<CodeBlockHandler>();
    }

    public void DetectGUIElementsUnderCursor()
    {
        m_PointerEventData = new PointerEventData(m_EventSystem);
        m_PointerEventData.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        m_Raycaster.Raycast(m_PointerEventData, results);

        foreach (RaycastResult result in results)
        {
            if(result.gameObject.tag == "GridElement") 
            {
                grid_ = result.gameObject;
            }
            if (result.gameObject.tag == COMMAND_TAG)
            {
                command_slot = result.gameObject;
            }
        }
    }
    
    private void SetForwardData(string data) 
    {
      clone_element_.transform.GetChild(0).GetComponent<Text>().resizeTextForBestFit = true;
      clone_element_.transform.GetChild(0).GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
      clone_element_.transform.GetChild(0).GetComponent<Text>().fontSize = 33;
      clone_element_.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(-25.0f, 0.0f);

      clone_element_.transform.GetChild(1).GetComponent<Text>().resizeTextForBestFit = true;
      clone_element_.transform.GetChild(1).GetComponent<Text>().text = data;
    }

  private void SetTurnData(string data)
  {
    clone_element_.transform.GetComponentInChildren<Text>().resizeTextForBestFit = true;
    clone_element_.transform.GetComponentInChildren<Text>().alignment = TextAnchor.MiddleCenter;
    clone_element_.transform.GetComponentInChildren<Text>().fontSize = 33;
    clone_element_.transform.GetComponentInChildren<RectTransform>().anchoredPosition = new Vector2(-25.0f, 0.0f);
    clone_element_.transform.GetComponentInChildren<RectTransform>().anchoredPosition = new Vector2(-25.0f, 0.0f);
    clone_element_.transform.GetComponentInChildren<Text>().text = data;
  }


    /*#################### Utils #################################### */

    private bool DraggingElementaryAction() 
    {
      return (command_prefab != null && this.gameObject.tag == ELEMENTARY_ACTION);
    }

    private bool isCloneExists() 
    {
        return clone_element_ != null;
    }

    private bool isCommandSlotExists() 
    {
      return command_slot != null;
    }

    private bool HasChild(GameObject obj) 
    {
        if(obj != null) 
        {
            return obj.transform.childCount > 0;
        }
        return true;
    }

    private void SetCenterAnchor(GameObject go)
    {
        go.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
        go.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
        go.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
    }

    private Vector2 GetCurrentMousePos()
    {
        return new Vector2(Input.mousePosition.x, Input.mousePosition.y);
    }

    private bool isInTarget()
    {
        return RectTransformUtility.RectangleContainsScreenPoint(target_area, Input.mousePosition);
    }

}
