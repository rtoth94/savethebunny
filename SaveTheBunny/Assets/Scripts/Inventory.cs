﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public Sprite CARROT_IMG;

    private List<GameObject> Inventory_items_ = new List<GameObject>();
    private int occupied_slots = 0;
    private Sprite original_img;

    private void GetItemFromSlots()
    {
        foreach (Transform child in this.transform)
         {
            if (child.tag == "Slot")
            {
                GameObject item = child.transform.GetChild(0).gameObject;
                Inventory_items_.Add(item);
            }
         }
    }
    void Start()
    {
        GetItemFromSlots();
        original_img = Inventory_items_[occupied_slots].GetComponentInChildren<Image>().sprite;
    }

    public void UseCarrot() 
    {
        occupied_slots--;
        if (occupied_slots >= 0 && occupied_slots < Inventory_items_.Count)
        {
            Inventory_items_[occupied_slots].GetComponentInChildren<Image>().sprite = original_img;
        }
        else
        {
            occupied_slots = 0;
        }
    }

    public void ResetSlots() 
    {
        for(int i = 0; i <= occupied_slots; i++) 
        {
            Inventory_items_[i].GetComponentInChildren<Image>().sprite = original_img;
        }
        occupied_slots = 0;
    }

    public void AddItemToSlot(GameObject item)
    {
        if(occupied_slots < Inventory_items_.Count)
        {
            Inventory_items_[occupied_slots].GetComponentInChildren<Image>().sprite = CARROT_IMG;
            occupied_slots++;
        }
        else
        {
            Debug.Log("Inventory Full");
        }
    }
}
