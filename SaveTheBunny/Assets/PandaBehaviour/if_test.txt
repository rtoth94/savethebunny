tree("Root")
	sequence
		repeat 1
			sequence
				Wait 0.2
				forward
				Wait 0.3
		sequence
			race
				while
					obstacle_ahead(1,0)
					repeat 1
						sequence
							Wait 0.5
							loop_forward
				while
					not obstacle_ahead(1,1)
					repeat 1
						sequence
							Wait 0.5
							loop_jump