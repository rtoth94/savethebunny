tree("Root")
	mute
		fallback
			while
				not obstacle_ahead(1,1)
				sequence
					repeat 1
						sequence
							Wait 0.5
							loop_turnleft
							Wait 0.3
					Wait 0.4
			repeat 1
				sequence
					Wait 0.2
					loop_forward
					Wait 0.3